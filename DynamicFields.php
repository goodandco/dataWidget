<?php

namespace app\components\widgets\dynamicFields;

use yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use app\models\user\User;
use app\components\widgets\dynamicFields\assets\DynamicFieldsAsset;

class DynamicFields extends Widget
{

    public $backend_model = 'app\components\widgets\dynamicFields\models\Backend';
    public $backend_method = 'run';
    public $frontend_model = 'app\components\widgets\dynamicFields\models\Frontend';
    public $frontend_method = 'run';
    public $options = [];
    public $value = null;
    protected $_data;
    protected $_html = '';

    public function init ()
    {
        Yii::$app->view->registerAssetBundle (DynamicFieldsAsset::className ());

        parent::init ();
    }

    public function run ()
    {
        $backend_model = new $this->backend_model();
        $backend_method = $this->backend_method;

        $frontend_model = new $this->frontend_model();
        $frontend_method = $this->frontend_method;
        
        $this->_data = $backend_model->$backend_method ($this->value, $this->options);

        $options = ($this->options) ? $this->options : [
            'label'       => Yii::t ('app', 'dynamic_field'),
            'name'        => 'DynamicField',
            'id'          => 'dynamic-field-item',
            'placeholder' => Yii::t ('app', 'specify'),
        ];        
        

        $this->_html .= $frontend_model->$frontend_method ($this->_data, $options);

        return $this->_html;
    }

}
