<?php

namespace app\components\widgets\dynamicFields\models;

class Frontend
{

    protected $_options = [];
    protected $_data = null;
    protected $_html = '';

    protected function _init ($data, $options)
    {
        $this->setData ($data);
        $this->setOptions ($options);
    }

    public function run ($data = null, $options = [])
    {
        return $this->response ();
    }

    public function response ()
    {
        return $this->getHtml ();
    }

    public function setData ($data)
    {
        $this->_data = $data;
    }

    public function getData ()
    {
        return $this->_data;
    }

    public function setOptions ($options)
    {
        $this->_options = $options;
    }

    public function getOptions ()
    {
        return $this->_options;
    }

    public function setHtml ($html)
    {
        $this->_html = $html;
    }

    public function getHtml ()
    {
        return $this->_html;
    }

    public function appendHtml ($html)
    {
        $this->_html .= $html;
    }

}
