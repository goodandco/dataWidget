<?php

namespace app\components\widgets\dynamicFields\models;

class Backend
{

    protected $_value = null;
    protected $_options = null;

    protected function _init ($value, $options)
    {
        ($this->getValue () === null) ? $this->setValue ($value) : null;
        ($this->getOptions () === null) ? $this->setOptions ($options) : null;
    }

    public function run ($value = null, $options = null)
    {
        $this->_init($value, $options);
        
        return $this->response ();
    }

    public function response ()
    {
        return $this->_value;
    }

    public function setOptions ($options)
    {
        $this->_options = $options;
    }

    public function getOptions ()
    {
        return $this->_options;
    }

    public function setValue ($data)
    {
        $this->_value = $data;
    }

    public function getValue ()
    {
        return $this->_value;
    }

}
