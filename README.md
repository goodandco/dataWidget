<h1>Using:</h1>
<p>
DynamicFields::widget ([<br />
    'backend_model' => 'my\model\namespace\BackendModel',<br />
    'frontend_model' => 'my\model\namespace\FrontendModel',<br />
    'value' => $some_data,<br />
    'options' => [<br />
        // some optional data<br />
    ],<br />
]);<br />
</p>

The 'backend_model' takes data from key 'value', process them and return to 'frontend_model'