<?php

namespace app\components\widgets\dynamicFields\assets;

use yii\web\AssetBundle;

class DynamicFieldsAsset extends AssetBundle
{

    public $sourcePath = '@app/components/widgets/dynamicFields/assets/';
    public $css = [];
    public $js = [
        'js/data-widget.js'
    ];
    public $depends = [];

}
